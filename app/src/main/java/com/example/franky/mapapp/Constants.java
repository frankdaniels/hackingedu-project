package com.example.franky.mapapp;

/**
 * Created by Franky on 10/24/2015.
 */
public final class Constants{
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME = "com.example.franky.mapapp";
    public static final String RECIEVER = PACKAGE_NAME + ".RECIEVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
}