package com.example.franky.mapapp;

        import android.app.IntentService;
        import android.content.Intent;
        import android.location.Address;
        import android.location.Geocoder;
        import android.location.Location;
        import android.os.Bundle;
        import android.os.ResultReceiver;
        import android.text.TextUtils;
        import android.util.Log;

        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Locale;

/**
 * Created by Franky on 10/24/2015.
 */
public class FetchAddressIntentService extends IntentService{
    protected ResultReceiver mReceiver;
    public FetchAddressIntentService(String name){
        super(name);
    }

    protected void onHandleIntent(Intent intent){
        String errorMessage = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Location location = intent.getParcelableExtra(Constants.LOCATION_DATA_EXTRA);
        List<Address> addresses = null;
        try{
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);

        } catch (IOException ioException){
            errorMessage = getString(R.string.service_not_available);
            Log.e("onHandleIntent", errorMessage, ioException);
        }catch (IllegalArgumentException illegalArgumentException){
            errorMessage = getString(R.string.illegal_lat_long_used);
            Log.e("getFromLocation", errorMessage + ". " + "Latitude = "
            + location.getLatitude() + ", longitude = " + location.getLongitude(),
                    illegalArgumentException);
        }

        if(addresses == null || addresses.size() == 0){
            if(errorMessage.isEmpty()){
                errorMessage = getString(R.string.no_address_found);
                Log.e("getFromLocation", errorMessage);
            }
            deliverResultToReciever(Constants.FAILURE_RESULT, errorMessage);
        }else{
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<String>();
            for(int i = 0; i < address.getMaxAddressLineIndex(); i++){
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i("getLocationFrom", getString(R.string.address_found));
            deliverResultToReciever(Constants.SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.seperator"),
                            addressFragments));
        }
    }
    private void deliverResultToReciever(int resultCode, String message){
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA_KEY, message);
        mReceiver.send(resultCode, bundle);
    }
}

